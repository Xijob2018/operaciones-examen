package com.examen.operaciones.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "User")
@Setter
@Getter
public class UserEntity {


    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="username")
    private String username;


    @Column(name = "password")
    private  String  password;

    @Column(name = "email")
    private  String  email;







}
