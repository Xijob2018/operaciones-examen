package com.examen.operaciones.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Book")
@Setter
@Getter

public class BookEntity {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;



    @Column(name = "title")
    private  String  title;

    @Column(name = "author")
    private  String  author;


    @Column(name = "isbn")
    private  String  isbn;



}
