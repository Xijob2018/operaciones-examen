package com.examen.operaciones.Controller;

import com.examen.operaciones.Entity.BookEntity;
import com.examen.operaciones.Repository.RepositoryBook;
import org.apache.logging.log4j.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ControllerBook {

    Logger logger = LoggerFactory.getLogger(ControllerBook.class);

    @Autowired
    RepositoryBook repositoryBook;


    @GetMapping("/allBook")
    public ResponseEntity<List>  allBook(){
        List bookList = new ArrayList();
        try {

            bookList = repositoryBook.findAll();
            return new ResponseEntity<>(bookList, HttpStatus.OK);
        }catch (Exception exception){
            logger.error(exception.getMessage());
            return new ResponseEntity<>(bookList, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @GetMapping("/booksId/{id}")
    public  Optional<BookEntity>  bookId(@PathVariable Long id){

        try {
            List bookList = new ArrayList();
            Optional<BookEntity> optionalBook = repositoryBook.findById(id);
            return optionalBook;
        }catch ( Exception exception ){
            logger.error("Error : "+ exception.getMessage());
            return  null;
        }

    }


    @GetMapping("/deleteId/{id}")
    public  String   deleteId(@PathVariable Long id){
        String message="";
        try {
            List bookList = new ArrayList();
            repositoryBook.deleteById(id);
            message="Registro borrado";
            return message;
        }catch ( Exception exception ){
            logger.error("Error : "+ exception.getMessage());
            return  exception.getMessage();
        }

    }



    @PostMapping("/actualizarBook")

    public String RegistrarTripulantes(@RequestBody BookEntity bookEntity){
        String Message="";
        BookEntity _book = repositoryBook
                .save(bookEntity);

        if(_book.getId()>0){
            logger.info("Se registro correctamente ");
            Message="Se registro correctamente";
        }else{
            logger.info("No Se registro correctamente el registro con el id  "+bookEntity.getId());
            Message="No Se registro correctamente el registro con el id  "+bookEntity.getId();
        }
        return Message;
    }













}
