package com.examen.operaciones.Controller;

import com.examen.operaciones.Entity.BookEntity;
import com.examen.operaciones.Entity.UserEntity;
import com.examen.operaciones.Repository.RepositoryBook;
import com.examen.operaciones.Repository.RepositoryUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ControllerUser {


    Logger logger = LoggerFactory.getLogger(ControllerBook.class);

    @Autowired
    RepositoryUser repositoryUser;


    @GetMapping("/allUser")
    public ResponseEntity<List> allUser(){
        List userList = new ArrayList();
        try {

            userList = repositoryUser.findAll();
            return new ResponseEntity<>(userList, HttpStatus.OK);
        }catch (Exception exception){
            logger.error(exception.getMessage());
            return new ResponseEntity<>(userList, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @GetMapping("/userId/{id}")
    public Optional<UserEntity> userId(@PathVariable Long id){

        try {
            List userList = new ArrayList();
            Optional<UserEntity> optionalList =repositoryUser.findById(id);
            return optionalList;
        }catch ( Exception exception ){
            logger.error("Error : "+ exception.getMessage());
            return  null;
        }

    }


    @GetMapping("/deleteId/{id}")
    public  String   deleteId(@PathVariable Long id){
        String message="";
        try {
            repositoryUser.deleteById(id);
            message="Registro borrado";
            return message;
        }catch ( Exception exception ){
            logger.error("Error : "+ exception.getMessage());
            return  exception.getMessage();
        }

    }



    @PostMapping("/actualizarUser")

    public String actualizarUser(@RequestBody UserEntity userEntity){
        String Message="";
        UserEntity _user = repositoryUser
                .save(userEntity);

        if(_user.getId()>0){
            logger.info("Se registro correctamente ");
            Message="Se registro correctamente";
        }else{
            logger.info("No Se registro correctamente el registro con el id  "+userEntity.getId());
            Message="No Se registro correctamente el registro con el id  "+userEntity.getId();
        }
        return Message;
    }





}
