package com.examen.operaciones.Repository;

import com.examen.operaciones.Entity.BookEntity;
import com.examen.operaciones.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryUser extends JpaRepository<UserEntity, Long> {

}
