package com.examen.operaciones.Repository;

import com.examen.operaciones.Entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryBook extends JpaRepository<BookEntity, Long> {

}
